# Android Apps On Linux

A toolchain to compile Android applications natively for Linux

> **Word of warning**: Such a toolchain doesn't exist yet, the intent of this repository is precisely to gather information on the feasability of the concept.

Because the project doesn't have a name yet, it will hereafter be refered to as "The Toolchain".

## Motivation

How many people are running emulators to run Android Apps on their PCs? What if Android developpers could package their app to run natively on Linux distributions without effort?

### Why not Anbox or $EMULATOR ?

[Anbox] or emulators are sufficient from the perspective of (savvy-enough) users that have no control over the source code of their Android apps:
They provide a simple way of running `.apk`-s on Desktop systems.

The Toolchain targets the opposite perspective, that of the developper.
It's about providing tools for continuous integration, deployment, and automated testing as well as a streamlined way to integrate into distro repositories, while sharing the same code base.
A parallel could be drawn, in a way, with Electron apps.

Of course, it would eventually also mean an improvement in user experience as well, because they would then be able to install cross-platform apps built with The Toolchain like any other native program (Even if the UI doesn't look native).
Similar to how TeamViewer for Linux is just TeamViewer for Windows but packaged with a compatible version of Wine.

It may also reduce the resource usage (CPU, RAM, storage) of such apps compared to running in [Anbox] or emulators.

### Why Linux ?

Well first of all, simply because I run Linux, and I'd like to run my Android apps on my Desktop.

But also because I believe this would be of particular interest to projects like

- [PostmarketOS](https://postmarketos.org/)
- [Ubuntu Touch](https://ubuntu-touch.io/)
- [PureOs](https://puri.sm/products/librem-5/pureos-mobile/)
- or [SailfishOS](https://sailfishos.org/)

in providing a simple way for developpers to write native apps for these OSes.

Additionally, amongst Desktop OSes, I believe (GNU/)Linux to be the closest to Android, and therefore the easiest target.
This is further indicated by the fact that the [Anbox] project (that doesn't use any hardware emulation) only runs on Linux (The two OSes share the same kernel after all).
See the [Rationale](#Rationale) for further detail.

## Rationale

So here goes the theory.
I'm no expert in Android development, nor operating systems development, so this is only my layman understanding, and please correct me if I'm wrong.

Android applications are mostly Java code.
Java is a cross-platform language.
So, theoretically, provided the appropriate libraries and functions are present, the code could stay the same while plugging in to a whole different system.

In other words, Android apps use a (more or less explicit) API.
If we can provide them with the same API, they should run the same.

In essence, this is what Wine does, except this would happen at compile-time, instead of at run-time. Or rather it would be more like what the Mono project initially did: allow C# source code intended for Windows to run on Linux and macOS.

So, technically speaking, the idea is: Instead of compiling to java bytecode, then to dalvik bytecode, and then trying to run the dalvik bytecode on Linux, why not just provide the jvm with the appropriate libraries to run the java bytecode produced in the first step of the compilation?
(Cf. [How Android Apps are Built and Run])

### Graphical User Interface

Android apps are mostly self-contained in terms of GUI: appart from the return button (and maybe a close button), all controls available to the user usually appear on-screen inside the app.
This means a simple universal wrapper could be written to implement that missing external functionality, and there would be virtually no porting effort required for the GUI.
(Unlike with [Multi-OS].)

Of course apps ported in that fashion wouldn't visually fit in with the rest of the Desktop Environment, but their usability should be left untouched.

## Links of interest

- [How Android Apps are Built and Run]
- [Anbox] (Android Runtime for Desktop Linux)
- [Android-x86] (Community port of the AOSP to x86 architectures)
- [Multi-OS] (Tool for developping apps for iOS and Android with a shared Java core)
- [ARChon] (Android Runtime through Chrome browser's engine)

[How Android Apps are Built and Run]: https://github.com/dogriffiths/HeadFirstAndroid/wiki/How-Android-Apps-are-Built-and-Run
[Anbox]: https://github.com/anbox/anbox
[Android-x86]: https://www.android-x86.org/documentation.html
[Multi-OS]: https://github.com/multi-os-engine/multi-os-engine
[ARChon]: https://github.com/vladikoff/chromeos-apk/blob/master/archon.md
